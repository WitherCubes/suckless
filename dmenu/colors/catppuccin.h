static const char *colors[SchemeLast][2] = {
	/*     fg         bg       */
	[SchemeNorm] = { "#f5c2e7", "#1e1e2e" },
	[SchemeSel] = { "#1e1e2e", "#f5c2e7" },
	[SchemeOut] = { "#1e1e2e", "#f9e2af" },
};
